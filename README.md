#Golang-Vagrant

A Vagrant configuration that starts up a server with Go + Postgres Database

### Installation

First install [Vagrant] and [Virtual Box].


### Usage

	# Clone it locally:
    $ git clone https://bitbucket.org/ngmachado/vagrant-golang/ server

    # Enter the cloned directory:
    $ cd server
    
    # Start up the virtual machine:
    $  vagrant up
    

### Some Vagrant commands
    # Start up the virtual machine:
    $  vagrant up
    
    # Connect to the virtual machine :
    $vagrant ssh
    
    # Exit ssh connection 
    $quit or crtl+C
    
    # Stop the virtual machine:
    $ vagrant halt

    # Destroy the virtual machine :
    $vagrant destroy
    
### License

This is released under the MIT license. See the file [LICENSE](LICENSE).

[Virtual Box]: https://www.virtualbox.org/
[Vagrant]: http://www.vagrantup.com/